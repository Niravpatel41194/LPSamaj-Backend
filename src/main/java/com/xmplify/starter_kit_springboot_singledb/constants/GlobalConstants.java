package com.xmplify.starter_kit_springboot_singledb.constants;

import java.net.URI;

public class GlobalConstants {

    public static final String STATUS_ACTIVE = "Active";
    public static final String REQUEST_SUCCESS = "Success";
    public static final String STATUS_SUCCESS = "Success";
    public static final String STATUS_FAILED = "Failed";
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_NORMAL = "Normal";
    public static final String BASIC_DETAIL = "Basic_Detail";
    public static final String ADDRESS = "Address";
    public static final String UPLOAD_IMAGE = "/WEB-INF/images/";
    public static final String UPLOAD_NEWS_MEDIA_FULL_PATH = UPLOAD_IMAGE+"news/";
    public static final String UPLOAD_NEWS_MEDIA_URL_PATH = "/images/news/";
    public static final String BACK_SLASH = "\\";

    public static final String NEWS_TYPE = "News";
    public static final String FILE_DOWNLOAD_HTTP_HEADER = "attachment; filename=\"%s\"";
}
