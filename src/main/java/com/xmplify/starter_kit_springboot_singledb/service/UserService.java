package com.xmplify.starter_kit_springboot_singledb.service;

import java.util.List;

import com.xmplify.starter_kit_springboot_singledb.model.User;

public interface UserService {

	 public List<User> findAll();
}
